# ide-setup Default config file for linter and code formattter

## Visual Studio Code Extension

1. We have create _Workspace Recommended Extensions_ in .vscode directory. Just copy this folder and put in root directory of a new project. When user open workspace for the first time vscode will prompt to install these extensions.
2. After first time user can always access these recommended extensions from `View -> Command Palette -> Extensions: Show Recommended Extensions` menu

## Linter and Code Formatter

1. Copy `.editorconfig`, `.eslintrc.json` and `.prettierrc` file to root directory of a new project
2. Install dev dependencies with command
   ```bash
   yarn add --dev eslint eslint-config-prettier eslint-config-standard-with-typescript eslint-plugin-node eslint-plugin-promise eslint-plugin-import @typescript-eslint/eslint-plugin prettier
   ```
3. For frontend project should install `prettier-plugin-tailwindcss` for automatic class name reordering
   ```bash
   yarn add --dev prettier-plugin-tailwindcss
   ```
4. Set default formatter to prettier go to`Code -> Preference -> Setting`. Search for `default formatter` and make sure it was set to `Prettier - Code Formatter`

## Force lint before commit

1. Install `husky` and `lint-staged` package `yarn add --dev husky husky-init lint-staged`
2. Init husky

   ```bash
   // yarn 1
   npx husky-init && yarn

   // yarn 2
   yarn dlx husky-init --yarn2 && yarn
   ```

3. Add following code to `package.json`
   ```json
   {
     ...,
     "lint-staged": {
       "src/**/*.{js,jsx,ts,tsx,scss,less}": [
         "prettier --write"
       ],
       "src/**/*.{js,jsx,ts,tsx}": [
         "eslint"
       ]
     },
     ...
   }
   ```
4. Change husky hook script in `.husky/pre-commmit` like so

   ```bash
   #!/bin/sh
   . "$(dirname "$0")/_/husky.sh"

   yarn build && yarn lint-staged
   ```
5. Set `.husky/pre-commit` to be executable 
   ```bash
   chmod +x .husky/pre-commit
   ```
